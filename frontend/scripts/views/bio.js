//'use strict';

var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;

var Base = require('./base');

module.exports = {

    Model: Backbone.Model.extend({
        defaults: {
            bio: ''
        }
    }),
    
    View: Base.View.extend({
        template: _.template($('#bio').html()),
        errorTemplate: _.template($('#bio-error').html()),
        loadingTemplate: _.template($('#loading').html()),

        bioLoaded: false,
        loadingError: false,

        render: function() {
            if(this.bioLoaded) {
                if(this.loadingError) {
                    this.$el.html(this.errorTemplate());
                }
                else {
                    this.$el.html(this.template({
                        bio: this.model.get('bio')
                    }));
                }
            }
            else {
                this.$el.html(this.loadingTemplate());
            }

            return this;
        }
    })
};
