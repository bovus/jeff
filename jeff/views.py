from pyramid.view import view_config

from jeff.services.facebook import FacebookService


#config = ConfigParser.ConfigParser()
#config.read('combat2college/config/config.ini')
accountId = 'biegelbass'#config.get('facebook', 'accountId')
accessToken = 'CAAJoWKtp33cBAGN04aL06J3xTz4FTcEowXZCFSU5SFk0RLkM3NgHTdBI7VZCqWiLHLBlDsjZAAvZCpsD9u9j5iWqZBZCrThn2GkDvZBC3oBtlJGd3cKHsCj9WbBMqx7lfbBPY2291mUDqSSlabWATuZAffPe4wW79NccMwR5846zVFUBvXFTBO6YQfa2aNuCZAePfGTJ7K13DeuhiRHUtNzDa'#config.get('facebook', 'accessToken')


@view_config(route_name='home', renderer='templates/main.pt')
def main(request):
    return {'project': 'jeff'}

@view_config(name='bio', renderer='string')
def bio(request):
    facebook = FacebookService(accountId, accessToken)
    bio = facebook.bio()
    return bio

@view_config(name='events', renderer='json')
def events(request):
    facebook = FacebookService(accountId, accessToken)
    events = facebook.upcoming_events(5)
    return events

@view_config(name='photos', renderer='json')
def photos(request):
    album_id = '10153158049525239'
    facebook = FacebookService(accountId, accessToken)
    photo_list = facebook.photos(album_id)
    return photo_list
