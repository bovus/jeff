//'use strict';

var $ = require('jquery');
var _ = require('underscore');
var youtube = require('youtube-iframe-player');

var Base = require('./base');

module.exports = {
    
    View: Base.View.extend({
        template: _.template( $('#video').html() ),
        
        render: function() {
            Base.View.prototype.render.call(this);
            
            return this;
        },    
    }),
    
    setupYoutube: function() {

        youtube.init(function() {
            var player = new youtube.createPlayer('player', {
                height: '100%',
                width: '100%',
                playerVars: {
                    listType: 'playlist',
                    list: 'PL0zbJgTvJ6N8iFKVFuO1B1s4RDv1XY6Sb'
                },
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange,
                    'onError': onPlayerError
                }
            });
        
            function onPlayerReady(event) {
                //event.target.playVideo();
            };

            function onPlayerStateChange(event) {
            
            };

            function onPlayerError(event) {
                //console.log(event);
            }
        });
    }

};
