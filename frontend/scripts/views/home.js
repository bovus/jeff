//'use strict';

var $ = require('jquery');
var _ = require('underscore');

var Base = require('./base');

module.exports = {
    
    View: Base.View.extend({
        template: _.template( $('#home').html() ),
        
        render: function() {
            Base.View.prototype.render.call(this);
            
            return this;
        }  
    })
};
