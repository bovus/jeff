//'use strict';

var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

module.exports = {
    
    View: Backbone.View.extend({
        tagName: 'div',

        render: function() {
            this.$el.html(this.template());
            return this;
        }
    })

};
