#setup
```
(jeff) $ pip install -r requirements.txt
(jeff) $ make init
```

#run server
```
(jeff) $ make dev
(jeff) $ python setup.py install
(jeff) $ pserver development.ini --reload
```