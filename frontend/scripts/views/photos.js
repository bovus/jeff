//'use strict';

var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;
require('slick-carousel');

var Base = require('./base');

module.exports = {

    Model: Backbone.Model.extend({
        defaults: {
            link: ''
        }
    }),
    
    View: Base.View.extend({
        template: _.template($('#photos').html()),
        errorTemplate: _.template($('#photos-error').html()),
        loadingTemplate: _.template($('#loading').html()),

        photosLoaded: false,
        loadingError: false,

        render: function() {

            if(this.photosLoaded) {
                if(this.loadingError || this.collection.length == 0) {
                    this.$el.html(this.errorTemplate());
                }
                else {
                    this.$el.html(this.template({
                        collection: this.collection.toJSON()
                    }));

                    this.$el.find('.photos').slick({
                        infinite: true,
                        arrows: false,
                        autoplay: true,
                        pauseOnHover: false
					});
                }
            }
            else {
                this.$el.html(this.loadingTemplate());
            }

            return this;
        }
    })
};
