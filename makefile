#
#   Jeffs website makefile
#

#backend
PYTHON=python

#frontend
FRONTEND_SRC=frontend
BUILD_DIR=jeff/static
TEMPLATE_DIR=jeff/templates

NPM_DIR=$(FRONTEND_SRC)/node_modules/.bin
#BOWER=node_modules/.bin/bower
JADE=$(NPM_DIR)/jade
BROWSERIFY=$(NPM_DIR)/browserify
UGLIFYJS=$(NPM_DIR)/uglifyjs
UGLIFYCSS=$(NPM_DIR)/uglifycss
LESS=$(NPM_DIR)/lessc
WATCH=$(NPM_DIR)/wr
UNCSS=$(NPM_DIR)/uncss
CLOSURE=$(NPM_DIR)/ccjs

init:
	(cd $(FRONTEND_SRC) && npm install) #&& $(BOWER) install)

watch:
	nohup sh -c '\
	$(WATCH) --exec "make dev-styles" $(FRONTEND_SRC)/styles && \
	$(WATCH) --exec "make dev-templates" $(FRONTEND_SRC)/templates && \
	$(WATCH) --exec "make dev-scripts" $(FRONTEND_SRC)/scripts && \
	$(WATCH) --exec "make dev-images" $(FRONTEND_SRC)/img && \
	$(WATCH) --exec "make dev-fonts" $(FRONTEND_SRC)/fonts \
	'
	
shitty-watch:
	$(WATCH) --exec 'make dev' $(FRONTEND_SRC)/templates $(FRONTEND_SRC)/scripts $(FRONTEND_SRC)/style

reset:
	mkdir -p $(BUILD_DIR) && \
	rm -rf $(BUILD_DIR)/* && \
	mkdir $(BUILD_DIR)/js $(BUILD_DIR)/css $(BUILD_DIR)/img $(BUILD_DIR)/fonts && \
	mkdir $(BUILD_DIR)/css/fonts

dev: reset dev-templates dev-scripts dev-styles dev-fonts dev-images

dev-templates:
	$(JADE) -o $(TEMPLATE_DIR) -O $(FRONTEND_SRC)/templates/dev.json -P $(FRONTEND_SRC)/templates/main.jade
	mv $(TEMPLATE_DIR)/main.html $(TEMPLATE_DIR)/main.pt

dev-scripts:
	$(BROWSERIFY) --debug $(FRONTEND_SRC)/scripts/main.js > $(BUILD_DIR)/js/bundle.js
	
dev-styles:
	$(LESS) --source-map-less-inline --source-map-map-inline --clean-css $(FRONTEND_SRC)/styles/main.less $(BUILD_DIR)/css/bundle.css

dev-images:
	cp -a $(FRONTEND_SRC)/images/. $(BUILD_DIR)/img

dev-fonts:
	cp $(FRONTEND_SRC)/fonts/* $(BUILD_DIR)/fonts && \
	cp $(FRONTEND_SRC)/node_modules/bootstrap/fonts/* $(BUILD_DIR)/fonts && \
	cp $(FRONTEND_SRC)/node_modules/font-awesome/fonts/* $(BUILD_DIR)/fonts && \
	cp $(FRONTEND_SRC)/node_modules/slick-carousel/slick/fonts/* $(BUILD_DIR)/css/fonts
	chmod 664 $(BUILD_DIR)/css/fonts/* && chmod 664 $(BUILD_DIR)/fonts/*
	
prod: reset
	$(JADE) -o $(TEMPLATE_DIR) -O $(FRONTEND_SRC)/templates/dev.json $(FRONTEND_SRC)/templates/main.jade && \
	mv $(TEMPLATE_DIR)/main.html $(TEMPLATE_DIR)/main.pt && \
	$(BROWSERIFY) $(FRONTEND_SRC)/scripts/main.js | $(UGLIFYJS) > $(BUILD_DIR)/js/bundle2.js && \
	$(CLOSURE) $(BUILD_DIR)/js/bundle2.js > $(BUILD_DIR)/js/bundle.js && \
	$(LESS) -clean-css $(FRONTEND_SRC)/styles/main.less | $(UGLIFYCSS) > $(BUILD_DIR)/css/bundle.css && \
	cp -a $(FRONTEND_SRC)/images/* $(BUILD_DIR)/img && \
	cp $(FRONTEND_SRC)/node_modules/bootstrap/fonts/* $(BUILD_DIR)/fonts && \
	cp $(FRONTEND_SRC)/node_modules/font-awesome/fonts/* $(BUILD_DIR)/fonts

#$(UNCSS) $(BUILD_DIR)/css/bundle.css > $(BUILD_DIR)/css/bundle.css && \
#cp server/config/prod.ini combat2college/config/config.ini
	
deploy:
	python setup.py build && \
	python setup.py install

shitty-deploy:
	make dev && make deploy && easy_install dist/jeff-0.0-py2.7.egg && \
	cp -r jeff/static/fonts /home/jason/.virtualenvs/jeff/lib/python2.7/site-packages/jeff-0.0-py2.7.egg/jeff/static
	cp -r jeff/static/css/fonts /home/jason/.virtualenvs/jeff/lib/python2.7/site-packages/jeff-0.0-py2.7.egg/jeff/static/css

.PHONY: dev
