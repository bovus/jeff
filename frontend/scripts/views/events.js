//'use strict';

var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
Backbone.$ = $;

var Base = require('./base');

module.exports = {

    Model: Backbone.Model.extend({
        defaults: {
            id: 0,
            name: '',
            time: ''
        }
    }),
    
    View: Base.View.extend({
        template: _.template($('#events').html()),
        noneTemplate: _.template($('#events-none').html()),
        errorTemplate: _.template($('#events-error').html()),
        loadingTemplate: _.template($('#loading').html()),

        eventsLoaded: false,
        loadingError: false,

        render: function() {

            if(this.eventsLoaded) {
                if(this.loadingError) {
                    this.$el.html(this.errorTemplate());
                }
                else if(this.collection.length == 0) {
                    this.$el.html(this.noneTemplate());
                }
                else {
                    this.$el.html(this.template({
                        collection: this.collection.toJSON()
                    }));
                }
            }
            else {
                this.$el.html(this.loadingTemplate());
            }

            //example of calling super view
            //Base.View.prototype.render.call(this);
            return this;
        }

    })
};
