'''
Sends HTTP requests and returns the response
'''

import urllib
import urllib2
try:
    import simplejson as json
except ImportError:
    import json
    
def getJson(url, params = None):
    '''
    makes a GET request, returns JSON
    '''
    
    if params is not None and len(params) > 0:
        url += '?' + urllib.urlencode(params)
    
    request = urllib2.Request(url)
    
    try:
        response = urllib2.urlopen(request)
    except:
        return None
    
    return json.loads(response.read())
    
def postJson(url, params):
    '''
    makes a POST request, returns JSON
    '''
    
    if params is None:
        params = {}
        
    headers = {'Content-Type': 'application/json'}
    request = urllib2.Request(url, json.dumps(params), headers)
    
    try:
        response = urllib2.urlopen(request)
    except:
        return None
    
    return json.loads(response.read())
