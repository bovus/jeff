//'use strict';

var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;
var bootstrap = require('bootstrap');
var Spinner = require('spin');

var Home = require('./views/home');
var Events = require('./views/events');
var Videos = require('./views/videos');
var Bio = require('./views/bio');
var Photos = require('./views/photos');

var EventsModel = Events.Model;
var BioModel = Bio.Model;
var PhotosModel = Photos.Model;

var EventsCollection = Backbone.Collection.extend({
    model: EventsModel
});
var PhotosCollection = Backbone.Collection.extend({
    model: PhotosModel
});

var HomeView = new Home.View();
var EventsView = new Events.View();
var VideosView = new Videos.View();
var BioView = new Bio.View();
var PhotosView = new Photos.View();

HomeView.render();
EventsView.render();
VideosView.render();
BioView.render();
PhotosView.render();

var soundcloud = SC.Widget($('#soundcloud').get(0));

var SpinnerOptions = require('./spinner-options');
var eventsSpinner = new Spinner(SpinnerOptions);
var bioSpinner = new Spinner(SpinnerOptions);
var photosSpinner = new Spinner(SpinnerOptions);

var Router = Backbone.Router.extend({
    routes: {
        '': 'home',
        'events': 'events',
        'videos': 'videos',
        'bio': 'bio',
		'photos': 'photos'
    },

    initialize: function() {
        this.on('route', function(route, param) {
            $('.nav li a').removeClass('active');
            if(route !== 'home') {
                $('#' + route + '-link').addClass('active');
            }
        });
    },

    home: function() {
        $('body').removeClass('black');
        $('#content').html(HomeView.$el);
    },
    
    events: function() { 
        $('body').removeClass('black');
        $('#content').html(EventsView.$el);
        if(!EventsView.eventsLoaded) {
            eventsSpinner.spin($('#spinner').get(0));
        }
    },

    videos: function() {
        soundcloud.pause();
        $('body').addClass('black');
        $('#content').html(VideosView.$el);
        Videos.setupYoutube();
    },

    bio: function() {
        $('body').removeClass('black');
        $('#content').html(BioView.$el);
        if(!BioView.bioLoaded) {
            bioSpinner.spin($('#spinner').get(0));
        }
    },

	photos: function() {
		$('body').removeClass('black');
		$('#content').html(PhotosView.$el);
		if(!PhotosView.photosLoaded) {
			photosSpinner.spin($('#spinner').get(0));
		}
	}
});

//https://stackoverflow.com/questions/8888491/how-do-you-display-javascript-datetime-in-12-hour-am-pm-format
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

//safari's Date.parse function doesn't work. fuck you safari!
//https://stackoverflow.com/a/6427318/1981635
function stringToDate(str) {
    var parts = str.split(/[^0-9]/);
    return new Date (parts[0], parts[1]-1, parts[2], parts[3], parts[4], parts[5]);
}

//https://stackoverflow.com/a/10124053/1981635
(function(){
    if (typeof Object.defineProperty === 'function'){
        try{Object.defineProperty(Array.prototype,'sortBy',{value:sb}); }catch(e){}
    }
    if (!Array.prototype.sortBy) Array.prototype.sortBy = sb;

    function sb(f) {
        for (var i=this.length;i;) {
            var o = this[--i];
            this[i] = [].concat(f.call(o,o,i),o);
        }
        this.sort(function(a,b) {
            for (var i=0,len=a.length;i<len;++i) {
                if (a[i]!=b[i]) return a[i]<b[i]?-1:1;
            }
            return 0;
        });
        for (var i=this.length;i;) {
            this[--i]=this[i][this[i].length-1];
        }
        return this;
  }
})();

(function facebookEvents() {
    $.get('events', function(eventsData) {

        if(eventsData === null) {
            EventsView.eventsLoaded = true;
            EventsView.loadingError = true;
            EventsView.render();
            return;
        }

        var tempEventItems = []

        for(var i = 0; i < eventsData.length; i++) {
            var e = eventsData[i];
            var d = stringToDate(e.start_time);//new Date(e.start_time);
            
            tempEventItems.push(
                {
                    id: e.id,
                    name: e.name,
                    date: d
                }
            );
        }

        tempEventItems.sortBy(function(i) { return i.date; });
        var eventItems = new EventsCollection();

        for(var i = 0; i < tempEventItems.length; i++) {
            var e = tempEventItems[i];
            var d = e.date;

            var eventItem = new EventsModel({
                id: e.id,
                name: e.name,
                time: (d.getMonth()+1) + '/' + d.getDate() + ' at ' + formatAMPM(d)
            });

            eventItems.add(eventItem);
        }

        EventsView.collection = eventItems;
        EventsView.eventsLoaded = true;
        EventsView.render();
        eventsSpinner.stop();
    });
})();

(function bioText() {
    $.get('bio', function(bio) {
        
        if(bio == null || bio == 'None') {
            BioView.bioLoaded = true;
            BioView.loadingError = true;
            BioView.render();
            return;
        }

        var model = new BioModel({ bio: bio });

        BioView.model = model;
        BioView.bioLoaded = true;
        BioView.render();
        bioSpinner.stop();
    });
})();

(function photos() {
    $.get('photos', function(photos) {
        
        if(photos == null || photos == 'None') {
            PhotosView.photosLoaded = true;
            PhotosView.loadingError = true;
            PhotosView.render();
            return;
        }

        var photoItems = new PhotosCollection();

        for(var i = 0; i < photos.length; i++) {
            var photoItem = new PhotosModel({
                link: photos[i]
            });

            photoItems.add(photoItem);
        }

        PhotosView.collection = photoItems;
        PhotosView.photosLoaded = true;
        PhotosView.render();
        photosSpinner.stop();
    });
})();

new Router();
Backbone.history.start();

$(function() {
    //collapse menu on click
    $(".navbar-collapse ul li a").click(function() {
        return $(".navbar-toggle:visible").click();
    });
});
