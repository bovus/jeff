'''
Interacts with Facebook in ways you can only imagine
'''

try:
    import simplejson as json
except ImportError:
    import json

from jeff.utils.http_requestor import getJson

class FacebookService(object):

    BASE_URL = 'https://graph.facebook.com/v2.3/'

    def __init__(self, account_id, access_token):
        self.account_id = account_id
        self.access_token = access_token

    def bio(self):
        '''
        return bio string from profile
        '''
        url = self.BASE_URL + self.account_id
        params = {
                    'fields': 'bio',
                    'access_token': self.access_token
                 }
        bioJson = getJson(url, params)

        if bioJson == None or 'error' in bioJson:
            return None

        return bioJson['bio']

                
    def upcoming_events(self, limit):
        '''
        returns JSON of upcoming events, returns None if there is a problem with API call
        '''
        url = self.BASE_URL + self.account_id + '/events'
        params = {
                    'format': 'json',
                    'method': 'get',
                    'since': 'now',
                    'until': 'next year',
                    'limit': limit,
                    'pretty': 0,
                    'access_token': self.access_token
                 }
         
        eventsJson = getJson(url, params)
        
        if eventsJson == None or 'error' in eventsJson:
            return None
        
        return eventsJson['data']

    def photos(self, album_id):
        '''
        returns a list of the highest resolution possible of photos in an album
        '''

        url = self.BASE_URL + album_id + '/photos'
        params = {
                    'access_token': self.access_token
                 }
        json = getJson(url, params)

        if json == None or 'error' in json:
            return None

        links = []
        for image in json['data']:
            links.append(image['images'][0]['source'])

        return links
